# Jottrbox

A docker container for a website that hosts simple Jotted HTML/JavaScript/CSS snippets.  Uses [Jotted](https://github.com/ghinda/jotted), PHP, Nginx, and JavaScript.  No database backend only json files.  The usernames and passwords are set in the users.config file in the jottrbox-user-info volume.  Set the admin line in the config to the username tht should have the right to edit the default file.

Jottrbox on bixfliz: https://jottrbox.bixfliz.com/.  

Jottrbox on Docker Hub: https://hub.docker.com/r/bixfliz/jottrbox.  

Example of launching jottrbox from the Docker Hub image:  
`docker run --restart=unless-stopped -d --name jottrbox -p 80:80 -v jottrbox-jots:/var/www/html/jots:rw -v jottrbox-user-info:/var/www/user-info:rw bixfliz/jottrbox:version1.??`  

Example of docker-compose.yaml:  
```
version: '3.4'

networks:
  default:

services:
  jottrbox:
    hostname: 'jottrbox'
    networks:
        default:
            aliases: 
                - jottrbox
    image: bixfliz/jottrbox:version1.0
    volumes:
      - jottrbox-jots:/var/www/html/jots:rw
      - jottrbox-user-info:/var/www/user-info:rw

volumes:
  jottrbox-jots:
  jottrbox-user-info;
```

Example Nginx reverse proxy for Jottrbox:
```


        server {
            listen 80;

            server_name jottrbox.bixfliz.com;
            return 301 https://jottrbox.bixfliz.com$request_uri;
        }

        server {
            listen              443 ssl;
            server_name jottrbox.bixfliz.com;
            keepalive_timeout   70;
            ssl on;

            ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
            ssl_ciphers         AES128-SHA:AES256-SHA:RC4-SHA:DES-CBC3-SHA:RC4-MD5;
            ssl_session_cache   shared:SSL:10m;
            ssl_session_timeout 10m;

            location  / {
                rewrite /(.*) /$1  break;
                proxy_pass http://jottrbox/;
                proxy_set_header   Host $host;
                proxy_redirect      http://jottrbox/ https://jottrbox.bixfliz.com/;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                sub_filter "http://jottrbox.bixfliz.com/" "https://jottrbox.bixfliz.com/";
                sub_filter_once off;
            }
        }
```

[Jotted](https://github.com/ghinda/jotted) is an environment for showcasing HTML, CSS and JavaScript, with editable source. It's like JSFiddle or JS Bin for self-hosted demos.  

**URL Formatting:**
* https://myjottrboxsite.com/ 
  * Shows the deflaut page.  
* https://myjottrboxsite.com/jots/username1-bootstrap-4-test 
  * Shows the user username1's bootstrap 4 test Jotted page filling the whole page without edit controls.  
* https://myjottrboxsite.com/?j=username1-bootstrap-4-test  
  * Shows the user username1's bootstrap 4 test Jotted page showing the edit controls for HTML, CSS, and JavaScript.  This will also show a login button for users that have account information in the api.php file.
* https://myjottrboxsite.com/?j=username1-bootstrap-4-test&s=pen
  * Same as above but uses style similar to codepen so that you can see the result, HTML, CSS, and JavaScript all at once.

Editing:  
  ![Screenshot](screenshots/edit-html.png)  
  ![Screenshot](screenshots/edit-result.png)  
  ![Screenshot](screenshots/edit-pen.png)  
Display:  
  ![Screenshot](screenshots/result.png)  

## Todo:

- [ ] Add support for authentication through FireBase
- [x] fix problem with deleteing jots
- [x] Convert to use Vue.js
- [x] Put Jottrbox on Docker Hub.
- [x] Put Jottrbox on Bixfliz.com
- [x] Provide a way to edit the default Jotted item. - added admin right in config
  - [x] Provide a way so that only certain users can edit the default Jotted item.
