<?php
function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}
function endsWith($haystack, $needle){
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function generateRandomString($length = 10) {
    $strValidCharactersForString = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $strValidCharactersForStringLength = strlen($strValidCharactersForString);
    $strNewRandomString = '';
    for ($i = 0; $i < $length; $i++) {
        $strNewRandomString .= $strValidCharactersForString[rand(0, $strValidCharactersForStringLength - 1)];
    }
    return $strNewRandomString;
}

function getUserIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        return $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        return $_SERVER['REMOTE_ADDR'];
    }
}

function deleteOldSessionsFilesSometimes(){
    // only do this 1 in 10 times
    if (rand(0, 10) != 5) return;
    $strSessionIDFolder = "/var/www/user-info/";
    if (file_exists($strSessionIDFolder)) {
        foreach (new DirectoryIterator($strSessionIDFolder) as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }
            if ($fileInfo->isFile() && time() - $fileInfo->getCTime() >= 10*24*60*60) {
                // delete session files older than 10 days
                unlink($fileInfo->getRealPath());
            }
        }
    }
}

try {
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $strAPIAction = $_POST["action"];
        if($strAPIAction === "login"){
            deleteOldSessionsFilesSometimes();
            $strUserInfoFile = "/var/www/user-info/users.config";

            if (!file_exists($strUserInfoFile)) {
                die("Can't find users.config.");
            }
            $arrUserAccounts = parse_ini_file($strUserInfoFile);

            $strCurrentUsername = isset($_POST['name']) ? $_POST['name'] : '';
            $strUserPasswordInput = isset($_POST['pass']) ? $_POST['pass'] : '';
            // get the password from the file that matches the username given
            $strTruePassword = $arrUserAccounts[$strCurrentUsername];
            if ($strUserPasswordInput == $strTruePassword) {
                $strSecretSessionID = generateRandomString();
                touch("/var/www/user-info/$strCurrentUsername-" . getUserIpAddr() . $strSecretSessionID);
                print  $strSecretSessionID;
            }else{
                // decrease chances of brute force attach
                sleep(5);
                die("x");
            }
        } elseif($strAPIAction === "login-check"){
            $strCurrentUsername = $_POST["name"];
            $strSecretSessionID = $_POST["key"];
            $strSessionIDFile = "/var/www/user-info/$strCurrentUsername-" . getUserIpAddr() . $strSecretSessionID;
            if (file_exists($strSessionIDFile) == FALSE) {
                die("login");
            }else{
                // file exists
                if (time()-filemtime($strSessionIDFile) > 48 * 3600) {
                    // file older than 48 hours
                    unlink($strSessionIDFile) or die("Couldn't delete file");
                    die("login");
                }else{
                    print "ok";
                }
            }
        } elseif($strAPIAction === "logout"){
            $strCurrentUsername = isset($_POST['name']) ? $_POST['name'] : '';
            $strSecretSessionID = isset($_POST['key']) ? $_POST['key'] : '';
            $strSessionIDFile = "/var/www/user-info/$strCurrentUsername-" . getUserIpAddr() . $strSecretSessionID;
            if (file_exists($strSessionIDFile) == FALSE) die("login");
            $strFolderContainingScript = dirname( __FILE__ );
            unlink($strSessionIDFile) or die("Couldn't delete file");
            print "ok";
        } elseif($strAPIAction === "delete"){
            $strCurrentUsername = $_POST["name"];
            $strSecretSessionID = $_POST["key"];
            $strJotFilename = $_POST["fileName"];
            $strSessionIDFile = "/var/www/user-info/$strCurrentUsername-" . getUserIpAddr() . $strSecretSessionID;
            if (file_exists($strSessionIDFile) == FALSE) {
                die("login");
            }else{
                if (time()-filemtime($strSessionIDFile) > 48 * 3600) {
                    // file older than 48 hours
                    unlink($strSessionIDFile) or die("Couldn't delete file");
                    die("login");
                }
            }

            if (startsWith($strJotFilename, $strCurrentUsername . "-") === false) {
                // not default and doesn't already have the username prefix
                $strJotFilename = $strCurrentUsername . "-" . $strJotFilename;
            }
            $strFolderContainingScript = dirname( __FILE__ );
            $strJotFilename = $strFolderContainingScript . "/jots/" . $strJotFilename;

            unlink($strJotFilename . ".json") or die("Couldn't delete file");
            unlink($strJotFilename . ".html") or die("Couldn't delete file");
            unlink($strJotFilename . ".css") or die("Couldn't delete file");
            unlink($strJotFilename . ".js") or die("Couldn't delete file");
            print "ok";
        } elseif($strAPIAction === "save"){
           $strCurrentUsername = $_POST["name"];
           $strSecretSessionID = $_POST["key"];
           $strJotFilename = $_POST["fileName"];
           $strSessionIDFile = "/var/www/user-info/$strCurrentUsername-" . getUserIpAddr() . $strSecretSessionID;
           if (file_exists($strSessionIDFile) == FALSE) {
              die("login");
           }else{
               if (time()-filemtime($strSessionIDFile) > 48 * 3600) {
                   // file older than 48 hours
                   unlink($strSessionIDFile) or die("Couldn't delete file");
                   die("login");
               }
          }

          if ($strJotFilename === "default") {
            $strUserInfoFile = "/var/www/user-info/users.config";
            $arrUserAccounts = parse_ini_file($strUserInfoFile);
            if  ($arrUserAccounts["admin"] !== $strCurrentUsername){
              die("not-admin" . $arrUserAccounts["admin"]);
            }
          }else{
              if (startsWith($strJotFilename, $strCurrentUsername . "-") === false) {
                  // not default and doesn't already have the username prefix
                  $strJotFilename = $strCurrentUsername . "-" . $strJotFilename;
              }
          }
          $strJotFileContent =  $_POST["fileData"];
          $strFolderContainingScript = dirname( __FILE__ );
          $strJotFilename = $strFolderContainingScript . "/jots/" . $strJotFilename . ".json";
          $jotFileObject = fopen($strJotFilename, "w") or die("Unable to open file: " . $strJotFilename);
          fwrite($jotFileObject, $strJotFileContent);
          fclose($jotFileObject);


        } elseif($strAPIAction === "save-files"){
          $strCurrentUsername = $_POST["name"];
          $strSecretSessionID = $_POST["key"];
          $strJotFilename = $_POST["fileName"];
          $strSessionIDFile = "/var/www/user-info/$strCurrentUsername-" . getUserIpAddr() . $strSecretSessionID;
          if (file_exists($strSessionIDFile) == FALSE) {
             die("login");
          }else{
              if (time()-filemtime($strSessionIDFile) > 48 * 3600) {
                  // file older than 48 hours
                  unlink($strSessionIDFile) or die("Couldn't delete file");
                  die("login");
              }
         }

         if ($strJotFilename !== "default") {
             if (startsWith($strJotFilename, $strCurrentUsername . "-") === false) {
                 // not default and doesn't already have the username prefix
                 $strJotFilename = $strCurrentUsername . "-" . $strJotFilename;
             }
         }



         $strPostload = '
         <script>
             document.addEventListener("DOMContentLoaded", function(){ 
             var fileref=document.createElement("script");
             fileref.setAttribute("type","text/javascript");
             fileref.setAttribute("src", "' . $strJotFilename. '.js");
             document.getElementsByTagName("head")[0].appendChild(fileref);
         
             var fileref=document.createElement("link");
             fileref.setAttribute("rel", "stylesheet");
             fileref.setAttribute("type", "text/css");
             fileref.setAttribute("href", "' . $strJotFilename. '.css");
             document.getElementsByTagName("head")[0].appendChild(fileref);
             document.getElementsByTagName("body")[0].style.display = "block";
            }, false);
         </script>';

         $strFolderContainingScript = dirname( __FILE__ );
         $strJotFilename = $strFolderContainingScript . "/jots/" . $strJotFilename;
         
         $strJotFileContent =  $_POST["fileDataHTML"];
         $strJotFileContent = preg_replace('/(<body.*?>)/i', '${1}<script>document.getElementsByTagName("body")[0].style.display = "none"; </script>', $strJotFileContent);
         $jotFileObject = fopen($strJotFilename . ".html", "w") or die("Unable to open file: " . $strJotFilename);
         fwrite($jotFileObject, $strJotFileContent . $strPostload);
         fclose($jotFileObject);

         $strJotFileContent =  $_POST["fileDataCSS"];
         $jotFileObject = fopen($strJotFilename . ".css", "w") or die("Unable to open file: " . $strJotFilename);
         fwrite($jotFileObject, $strJotFileContent);
         fclose($jotFileObject);

         $strJotFileContent =  $_POST["fileDataJS"];
         $jotFileObject = fopen($strJotFilename . ".js", "w") or die("Unable to open file: " . $strJotFilename);
         fwrite($jotFileObject, $strJotFileContent);
         fclose($jotFileObject);

        }elseif($strAPIAction === "list"){
          $strFolderContainingScript = dirname( __FILE__ ) . "/jots";
          $oAllJotFiles = scandir($strFolderContainingScript);
          $oAllJotFiles = array_diff(scandir($strFolderContainingScript), array('.', '..'));

          foreach ($oAllJotFiles as &$oEachJotFile) {
            if (endsWith($oEachJotFile,".json")){
              // json is the main data file for jotted
              $strJotFilenameWithoutExtension = str_replace(".json", "", $oEachJotFile);
              if (file_exists("jots/" . $strJotFilenameWithoutExtension . ".html")) {
                // the html file is the file meant for viewing
                print  "<a href=\"?j=" . $strJotFilenameWithoutExtension . 
                "\" title='edit jot'><i class='fa fa-pencil' aria-hidden='true'></i> " . 
                $strJotFilenameWithoutExtension . "</a>&nbsp; <a href=\"jots/" . $strJotFilenameWithoutExtension . 
                ".html\" title='view readonly'><i class='fa fa-file-text' aria-hidden='true'></i></a><br>";
              }else{
                print "<a href=\"?j=" . $strJotFilenameWithoutExtension . 
                "\" title='edit jot'><i class='fa fa-pencil' aria-hidden='true'></i> " . 
                $strJotFilenameWithoutExtension ."</a>&nbsp; <br>";
              }
            }
          }
        }
    }else{
      header('HTTP/1.0 403 Forbidden');
    }
} catch (Exception $e) {
    print 'Caught exception: ' .  $e->getMessage();
}
?>
