'use strict';
/* eslint-disable require-jsdoc */
const strVersion = '1.16.0';
let strUserInputHTML = '';
let strUserInputCSS = '';
let strUserInputJS = '';
let strCurrentSessionID = '';

function checkLogin() {
  if (!vue.username) vue.username = getCookie('user');
  if (!strCurrentSessionID) strCurrentSessionID = getCookie('session');
  if (!vue.username) setLoginState(false);

  const oFormDataForAPIPost = new FormData();
  const oAPIPostRequest = new XMLHttpRequest();
  oFormDataForAPIPost.append('action', 'login-check');
  oFormDataForAPIPost.append('name', vue.username);
  oFormDataForAPIPost.append('key', strCurrentSessionID);
  oAPIPostRequest.open('POST', 'api.php');
  oAPIPostRequest.send(oFormDataForAPIPost);

  oAPIPostRequest.onreadystatechange = function() {
    if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
      if (oAPIPostRequest.responseText === 'ok') {
        setLoginState(true);
      } else {
        setLoginState(false);
      }
    }
  };
}

function setLoginState(IsLoggedIn) {
  if (!vue.username) vue.username = getCookie('user');
  vue.loggedIn = IsLoggedIn;
  if (IsLoggedIn) {
    vue.pageTitleLabel = document.title + ' [' + vue.username + ']';
  } else {
    setCookie('session', '', 4);
    setCookie('user', '', 4);
    vue.pageTitleLabel = document.title;
  }
}

function setCookie(cname, cvalue, exdays) {
  const cookieExpireDate = new Date();
  cookieExpireDate.setTime(
      cookieExpireDate.getTime() + exdays * 24 * 60 * 60 * 1000
  );
  const expires = 'expires=' + cookieExpireDate.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
  const name = cname + '=';
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    // c is for cookie and that's good enough for me
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

const vue = new Vue({
  el: '#app',
  data: {
    jottrboxVersion: strVersion,
    loggedIn: false,
    pageTitleLabel: 'Jottrbox',
    currentFilename: '',
    currentFileTitle: '',
    fileList: '',
    username: '',
    password: '',
  },
  methods: {
    pageLoded: function() {
      vue.jottrboxVersion = 'Jottrbox: ' + strVersion;

      if (!vue.username) vue.username = getCookie('user');
      if (!strCurrentSessionID) strCurrentSessionID = getCookie('session');
      checkLogin();
      const oURLQueryString = new URLSearchParams(window.location.search);

      vue.showDeleteButton = false;

      if (oURLQueryString.has('j')) {
        vue.currentFilename = oURLQueryString.get('j');
      } else {
        vue.currentFilename = 'default';
      }
      if (vue.currentFilename !== 'default') {
        let strFilenameNoUser = vue.currentFilename;
        if (strFilenameNoUser.startsWith(vue.username + '-')) {
          strFilenameNoUser = strFilenameNoUser.replace(
              vue.username + '-',
              ''
          );
        }
        vue.currentFileTitle = strFilenameNoUser;
      }

      const requestJot = new XMLHttpRequest();
      requestJot.open('GET', 'jots/' + vue.currentFilename + '.json', true);
      requestJot.onload = function() {
        if (requestJot.status >= 200 && requestJot.status < 400) {
          // Success!
          let oJot = null;
          let oJotted = null;
          oJot = JSON.parse(requestJot.responseText);
          strUserInputHTML = oJot.files.find(
              (htmlItem) => htmlItem.type === 'html').content;
          strUserInputCSS = oJot.files.find((cssItem) => cssItem.type === 'css')
              .content;
          strUserInputJS = oJot.files.find((jsItem) => jsItem.type === 'js')
              .content;

          if (oURLQueryString.has('s')) {
            if (oURLQueryString.get('s') === 'pen') {
              oJot.plugins = ['ace', 'pen'];
            }
          }

          oJotted = new Jotted(
              document.querySelector('#jotted-demo-ace'), oJot);
          oJotted.on('change', function(params, callback) {
            if (params.type === 'html') {
              strUserInputHTML = params.content;
            } else if (params.type === 'css') {
              strUserInputCSS = params.content;
            } else if (params.type === 'js') {
              strUserInputJS = params.content;
            }
            callback(null, params);
          });
          checkLogin();

          /* if (oURLQueryString.get('e') !== 'y') {
            // hide everything so that only the result shows
            document.getElementsByTagName('iframe')[0].className =
              'fullscreen-overlay';
            document.getElementsByTagName('header')[0].className = 'd-none';
            document.getElementsByTagName('footer')[0].className = 'd-none';
            document.getElementsByClassName(
                'jotted-nav')[0].className = 'd-none';
            $('#jotted-demo-ace').style.height = '100vh';
          }*/
          document.getElementsByTagName('body')[0].className = '';
        } else {
          // We reached our target server, but it returned an error
          bootbox.alert(vue.currentFilename + ' not found.');
          document.getElementsByTagName('body')[0].className = '';
        }
      };

      requestJot.onerror = function() {
        // There was a connection error of some sort
        bootbox.alert(vue.currentFilename + ' not found.');
        document.getElementsByTagName('body')[0].className = '';
      };

      requestJot.send();
    },
    changeFilename: function() {
      bootbox.prompt({
        title: 'Set the name of the jot.',
        backdrop: true,
        value: vue.currentFilename,
        callback: function(result) {
          if (result !== null) {
            vue.currentFilename = result;
          }
        },
      });
    },
    listFiles: function() {
      const oFormDataForAPIPost = new FormData();
      const oAPIPostRequest = new XMLHttpRequest();
      oFormDataForAPIPost.append('action', 'list');
      oAPIPostRequest.open('POST', 'api.php');
      oAPIPostRequest.send(oFormDataForAPIPost);

      oAPIPostRequest.onreadystatechange = function() {
        if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
          vue.fileList = oAPIPostRequest.responseText;
        }
      };
    },
    logout: function() {
      if (!vue.username) vue.username = getCookie('user');
      if (!strCurrentSessionID) strCurrentSessionID = getCookie('session');

      if (!vue.username) {
        bootbox.alert('Please login first.');
        return;
      }

      const oFormDataForAPIPost = new FormData();
      const oAPIPostRequest = new XMLHttpRequest();
      oFormDataForAPIPost.append('action', 'logout');
      oFormDataForAPIPost.append('name', vue.username);
      oFormDataForAPIPost.append('key', strCurrentSessionID);
      oAPIPostRequest.open('POST', 'api.php');
      oAPIPostRequest.send(oFormDataForAPIPost);

      oAPIPostRequest.onreadystatechange = function() {
        if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
          setLoginState(false);
        }
      };
    },
    login: function() {
      setLoginState(false);

      const oFormDataForAPIPost = new FormData();
      const oAPIPostRequest = new XMLHttpRequest();
      oFormDataForAPIPost.append('action', 'login');
      oFormDataForAPIPost.append('name', vue.username);
      oFormDataForAPIPost.append('pass', vue.password);
      oAPIPostRequest.open('POST', 'api.php');
      oAPIPostRequest.send(oFormDataForAPIPost);

      oAPIPostRequest.onreadystatechange = function() {
        if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
          if (oAPIPostRequest.responseText !== 'x') {
            // user is logged in
            setCookie('session', oAPIPostRequest.responseText, 4);
            setCookie('user', vue.username, 4);
            strCurrentSessionID = oAPIPostRequest.responseText;
            vue.username = vue.username;
            setLoginState(true);
            $('#modalLoginForm').modal('hide');
          } else {
            setLoginState(false);
            showToastMessage('Login failed.', 2500);
          }
        }
      };
    },
    deleteFile: function() {
      if (!vue.username) vue.username = getCookie('user');
      if (!strCurrentSessionID) strCurrentSessionID = getCookie('session');
      checkLogin();

      if (!vue.username) {
        bootbox.alert('Please login first.');
        return;
      }

      const strDeleteFilename = vue.currentFilename;
      if (strDeleteFilename === '') {
        bootbox.alert('No current item to delete.');
        return;
      }
      if (strDeleteFilename === 'default') {
        bootbox.alert('You can\'t delete the default file.');
        return;
      }
      bootbox.confirm('Delete ' + vue.currentFilename + '?',
          function(result) {
            if (result) {
              const oFormDataForAPIPost = new FormData();
              const oAPIPostRequest = new XMLHttpRequest();
              oFormDataForAPIPost.append('action', 'delete');
              oFormDataForAPIPost.append('name', vue.username);
              oFormDataForAPIPost.append('key', strCurrentSessionID);
              oFormDataForAPIPost.append('fileName', vue.currentFilename);
              oAPIPostRequest.open('POST', 'api.php');
              oAPIPostRequest.send(oFormDataForAPIPost);

              oAPIPostRequest.onreadystatechange = function() {
                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                  if (oAPIPostRequest.responseText === 'login') {
                    bootbox.alert('Please login first.');
                    setLoginState(false);
                    vue.pageTitleLabel = document.title;
                    return;
                  } else if (oAPIPostRequest.responseText === 'ok') {
                    if (typeof history.pushState !== 'undefined') {
                      window.history.pushState('', '', '?');
                    }
                    bootbox.alert(vue.currentFilename + ' deleted.'
                    );
                  } else {
                    bootbox.alert(
                        'Unable to delete ' + vue.currentFilename +
                    '.'
                    );
                  }
                }
              };
            }
          }
      );
    },
    saveFile: function() {
      if (!vue.username) vue.username = getCookie('user');
      if (!strCurrentSessionID) strCurrentSessionID = getCookie('session');
      checkLogin();

      if (!vue.username) {
        bootbox.alert('Please login first.');

        return;
      }
      const oJottedUserInput = {
        files: [
          {
            type: 'html',
            content: strUserInputHTML,
          },
          {
            type: 'css',
            content: strUserInputCSS,
          },
          {
            type: 'js',
            content: strUserInputJS,
          },
        ],
        pane: 'result',
        showBlank: true,
        plugins: ['ace'],
      };
      const strSaveFilename = vue.currentFilename;
      if (strSaveFilename === '') {
        bootbox.alert('Please enter a name for this jot.');
        return;
      }
      const oFormDataForAPIPost = new FormData();
      const strJottedUserInput = JSON.stringify(oJottedUserInput);
      const oAPIPostRequest = new XMLHttpRequest();
      oFormDataForAPIPost.append('action', 'save');
      oFormDataForAPIPost.append('name', vue.username);
      oFormDataForAPIPost.append('key', strCurrentSessionID);
      oFormDataForAPIPost.append('fileName', strSaveFilename);
      oFormDataForAPIPost.append('fileData', strJottedUserInput);
      oAPIPostRequest.open('POST', 'api.php');

      oAPIPostRequest.onreadystatechange = function() {
        if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
          if (oAPIPostRequest.responseText === 'not-admin') {
            showToastMessage('You need admin rights to save this file.', 2500);
            return;
          } else if (oAPIPostRequest.responseText === 'login') {
            showToastMessage('Please login first.', 2500);
            setLoginState(false);
            return;
          } else {
            setLoginState(true);
            showToastMessage('File <b>' + strSaveFilename +
                '</b> saved.', 2500);

            const oFormDataForAPIPostFiles = new FormData();
            const oAPIPostRequestFiles = new XMLHttpRequest();
            oFormDataForAPIPostFiles.append('action', 'save-files');
            oFormDataForAPIPostFiles.append('name', vue.username);
            oFormDataForAPIPostFiles.append('key', strCurrentSessionID);
            oFormDataForAPIPostFiles.append('fileName', strSaveFilename);
            oFormDataForAPIPostFiles.append('fileDataHTML', strUserInputHTML);
            oFormDataForAPIPostFiles.append('fileDataCSS', strUserInputCSS);
            oFormDataForAPIPostFiles.append('fileDataJS', strUserInputJS);
            oAPIPostRequestFiles.open('POST', 'api.php');
            oAPIPostRequestFiles.send(oFormDataForAPIPostFiles);

            oAPIPostRequestFiles.onreadystatechange = function() {
              if (oAPIPostRequestFiles.readyState === XMLHttpRequest.DONE) {
                if (oAPIPostRequestFiles.responseText === 'login') {
                  showToastMessage('Please login first.', 2500);
                  setLoginState(false);
                  return;
                } else {
                  setLoginState(true);
                }
              }
            };
          }
        }
      };
      oAPIPostRequest.send(oFormDataForAPIPost);

      if (typeof history.pushState !== 'undefined') {
        let strModifiedBrowserURL = '';
        if (strSaveFilename === 'default') {
          // any user can save to the default document
          strModifiedBrowserURL = '?j=' + strSaveFilename;
        } else if (!strSaveFilename.startsWith(vue.username + '-')) {
          strModifiedBrowserURL =
            '?j=' + vue.username + '-' + strSaveFilename;
        } else {
          strModifiedBrowserURL = '?j=' + strSaveFilename;
        }
        window.history.pushState('', '', strModifiedBrowserURL);
      }
    },
  },
});

vue.pageLoded();

function showToastMessage(message, milliseconds) {
  const toast = bootbox.alert({
    message: message,
    backdrop: true,
    size: 'small',
  });

  setTimeout(function() {
    toast.modal('hide');
  }, milliseconds);
}
